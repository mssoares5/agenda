package app;

import com.google.gson.Gson;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.Provider;
import org.apache.commons.beanutils.BeanUtils;

/**
 * Response filter to allow for partial JSON responses
 * 
 * @author miguel
 */
@Provider
public class PartialResponseFilter implements ContainerResponseFilter {
    
    private final static String FIELDS = "fields";

    /**
     * Filter method called after a response has been provided for a request. 
     * Filters the response JSON based on the query parameter "fields"
     * 
     * @param requestContext request context
     * @param responseContext response context
     */
    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext){
        List<String> fields = getFields(requestContext);
        Object entity = responseContext.getEntity();
        Gson gson = new Gson();
        
        if (fields.size() > 0) {
            if(entity instanceof Collection){
                Collection filteredList=new ArrayList<>();
                for(Object obj : (Collection)entity){
                    filteredList.add(this.filterEntityV2(obj, fields));
                }
                responseContext.setEntity(gson.toJson(filteredList));
            }else{
                Object filteredEntity = gson.toJson(this.filterEntityV2(entity, fields));
                responseContext.setEntity(filteredEntity);
            }
        }
    }

    /**
     * Removes from Entity all keys not contained in fields list.
     *
     * @param object Entity converted to a map
     * @param fields list of permitted fields
     */
    private Object filterEntityV2(Object object, List<String> fields) {
        Map<String, String> map;
        try {
            map = BeanUtils.describe(object);
            final String keys[] = map.keySet().toArray(new String[]{});

            for (String key : keys) {
                if (!fields.contains(key)) {
                    map.remove(key);
                }
            }
        } catch (InvocationTargetException | NoSuchMethodException | IllegalArgumentException | IllegalAccessException ex) {
            return object;
        }
        return map;
    }

    /**
     * Retrieves the list of fields that user wants.
     *
     * @return List of fields
     */
    private List<String> getFields(ContainerRequestContext requestContext) {
        final MultivaluedMap<String, String> parameters = requestContext.getUriInfo().getQueryParameters();
        final String fields = parameters.getFirst(FIELDS);

        return fields != null ? Arrays.asList(fields.split(",")) : new ArrayList<String>();
    }
}
