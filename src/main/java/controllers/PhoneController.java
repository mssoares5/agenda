package controllers;

import java.net.URI;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import models.Contact;
import models.Phone;

/**
 * Phone sub-resources
 * @author miguel.soares
 */
@Stateless
public class PhoneController extends AbstractController<Phone> {

    @PersistenceContext(unitName = "ubi_agenda_war_v1PU")
    private EntityManager em;
    @Context
    private UriInfo context;
    
    public PhoneController() {
        super(Phone.class);
    }

    /**
     * Creates a new phone for a contact
     * 
     * @param entity phone to be persisted
     * @return Response with URI of the created contact in the Location HTTP header and JSON representation of the contact created. 
     * 
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createPhone(Phone entity) {
        String id = context.getPathParameters().getFirst("contactid");
        Contact contact=em.find(Contact.class, Integer.parseInt(id));
        entity.setContactid(contact);
        Phone created=((Phone) super.create(entity).getEntity());
        URI absolutePath = context.getAbsolutePath();
        URI createdURI= absolutePath.resolve(created.getId().toString());
        return Response.created(createdURI).entity(created).build();
    }
    
    /**
     * Updates a phone
     * 
     * @param id ID of the phone to be updated
     * @param entity Phone with the updates
     * @return Response with URI of the updated phone in the Location HTTP header and JSON representation of the phone updated.  
     */
    @PUT
    @Path("{id}")
    @Consumes( MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response edit(@PathParam("id") Integer id, Phone entity) {
        entity.setId(id);
        super.edit(entity);
        URI absolutePath = context.getAbsolutePath();
        URI createdURI= absolutePath.resolve(entity.getId().toString());
        return Response.created(createdURI).entity(entity).build();
    }
    
    /**
     * Deletes a phone
     * 
     * @param id ID of the phone to be deleted
     */
    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        super.remove(super.find(id));
    }
    
    /**
     * Finds the contact with the given ID
     * 
     * @param id ID of the phone to be found
     * @return Phone with the given ID.
     */
    @GET
    @Path("{id}")
    @Produces( MediaType.APPLICATION_JSON)
    public Phone find(@PathParam("id") Integer id) {
        return super.find(id);
    }
    
    /**
     * 
     * @return All phones
     */
    @GET
    @Override
    @Produces(MediaType.APPLICATION_JSON)
    public List<Phone> findAll() {
        String id = context.getPathParameters().getFirst("contactid");
        Contact contact=em.find(Contact.class, Integer.parseInt(id));
        List resultList = em.createNamedQuery("Phone.findByContact").setParameter("contactid", contact).getResultList();
        
        return resultList;
        
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
