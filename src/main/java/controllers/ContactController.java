package controllers;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import models.Contact;

/**
 * Contact resources
 * @author miguel.soares
 */
@Stateless
@Path("contacts")
public class ContactController extends AbstractController<Contact> {

    @PersistenceContext(unitName = "ubi_agenda_war_v1PU")
    private EntityManager em;
    @Context
    private UriInfo context;
    @EJB private PhoneController phoneController;

    public ContactController() {
        super(Contact.class);
    }

    /**
     * Creates a new contact
     * 
     * @param entity Contact to be persisted
     * @return Response with URI of the created contact in the Location HTTP header and JSON representation of the contact created. 
     * If the contact email already exists a BAD REQUEST status is returned
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public Response create(Contact entity) {
        try{
            Contact created=((Contact) super.create(entity).getEntity());
            em.flush();
            URI absolutePath = context.getAbsolutePath();
            URI createdURI= absolutePath.resolve(created.getId().toString());
            return Response.created(createdURI).entity(created).build();
        }catch(PersistenceException ex){
            return Response.status(Response.Status.BAD_REQUEST).entity("The email must be unique for each contact").build();
        }
        
    }

    /**
     * Updates a contact
     * 
     * @param id ID of the contact to be updated
     * @param entity Contact with the updates
     * @return Response with URI of the updated contact in the Location HTTP header and JSON representation of the contact updated.  
     */
    @PUT
    @Path("{contactid}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response edit(@PathParam("contactidid") Integer id, Contact entity) {
        entity.setId(id);
        super.edit(entity);
        URI absolutePath = context.getAbsolutePath();
        URI createdURI= absolutePath.resolve(entity.getId().toString());
        return Response.created(createdURI).entity(entity).build();
    }

    /**
     * Deletes a contact
     * 
     * @param id ID of the contact to be deleted
     */
    @DELETE
    @Path("{contactid}")
    public void remove(@PathParam("contactidid") Integer id) {
        super.remove(super.find(id));
    }

    /**
     * Finds the contact with the given ID
     * 
     * @param id ID of the contact to be found
     * @return Contact with the given ID.
     */
    @GET
    @Path("{contactid}")
    @Produces(MediaType.APPLICATION_JSON)
    public Contact find(@PathParam("contactid") Integer id) {
        return super.find(id);
    }
    
    /**
     * Phone Sub-resource locator
     * 
     * @param id Contact ID 
     * @return Phone resources
     */
    @Path("{contactid}/phones")
    public PhoneController findPhones(@PathParam("contactidid") Integer id) {
        return this.phoneController;
    }

    /**
     * 
     * @return All contacts in the agenda
     */
    @GET
    @Override
    @Produces(MediaType.APPLICATION_JSON)
    public List<Contact> findAll() {
        List<Contact> contacts = super.findAll();
        return contacts;
    }

    /**
     * Gets a predefined number of contacts  
     * 
     * @param from start index
     * @param to end index
     * @return All contacts from start index until end index
     */
    @GET
    @Path("{from}/{to}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Contact> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    /**
     * 
     * @return Number of contacts in the agenda
     */
    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        return String.valueOf(super.count());
    }
    
    /**
     * Search based on query parameters
     * 
     * @return Contacts that meet the criteria
     */
    @GET
    @Path("/search")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Contact> getContactByPartialName() {
        MultivaluedMap<String, String> queryParameters = context.getQueryParameters();
        
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Contact> query = cb.createQuery(Contact.class);
        Root<Contact> contact = query.from(Contact.class);
        List<Predicate> predicates = new ArrayList<>();
        try{
            for(String key : queryParameters.keySet()){
                predicates.add(cb.equal(contact.get(key), queryParameters.get(key)));
            }
        }catch(java.lang.IllegalArgumentException ex){
            System.out.println("Bad search Criteria");
        }
        query.where(cb.and(predicates.toArray(new Predicate[predicates.size()])));
        
        return em.createQuery(query).getResultList();
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
